import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { TextSummary } from "./components";

function App() {
  return (
    <div className="App">
      <TextSummary
        loading
        title="Testes101"
        description="lorem ispum doller sit etc..."
      />
      <TextSummary
        title="Testes101"
        description="lorem ispum doller sit etc..."
      />
    </div>
  );
}

export default App;

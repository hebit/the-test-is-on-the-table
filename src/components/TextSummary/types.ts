export type TextSummaryProps = {
  loading?: boolean;
  title: string;
  description: string;
};

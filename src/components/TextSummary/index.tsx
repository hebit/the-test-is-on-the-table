import { useState } from "react";

import { TextSummaryProps } from "./types";

export function TextSummary({ loading, title, description }: TextSummaryProps) {
  const [isExpanded, setIsExpanded] = useState(false);

  if (loading) {
    return (
      <div className="text-summary-container">
        <div
          className="book-icon-box loading loading-animation"
          data-testid="loading"
        >
          <div className="left">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div className="swap">
            <span></span>
            <span></span>
          </div>
          <div className="right">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <span data-testid="content">Loading...</span>
      </div>
    );
  }
  return (
    <div className="text-summary-container">
      <div className="book-icon-box">
        <div className="left">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div className="swap">
          <span></span>
          <span></span>
        </div>
        <div className="right">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <details data-testid="content" onClick={() => setIsExpanded(true)}>
        <summary data-testid="content-title">{title}</summary>
        {isExpanded && <p data-testid="content-description">{description}</p>}
      </details>
    </div>
  );
}

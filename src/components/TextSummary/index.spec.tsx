import { render, screen, fireEvent } from "@testing-library/react";
import { TextSummary } from ".";

describe("TextSummary", () => {
  const title = "Titulo de Teste";
  const description =
    "Não obstante, a adoção de políticas descentralizadoras agrega valor ao estabelecimento de todos os recursos funcionais envolvidos.";

  it("should show loading book animation if loading is true", () => {
    render(
      <TextSummary loading={true} title={title} description={description} />
    );
    const loadingElement = screen.queryByTestId("loading");

    expect(loadingElement).toBeInTheDocument();
  });

  it("should show title if loading is not true", () => {
    render(<TextSummary title={title} description={description} />);
    const contentElement = screen.queryByTestId("content");

    expect(contentElement).toHaveTextContent(title);
  });

  it("should show description when click on title", () => {
    render(<TextSummary title={title} description={description} />);

    const titleElement = screen.queryByTestId("content-title");

    expect(titleElement).not.toBeNull();
    if (titleElement) {
      fireEvent.click(titleElement);
    }

    const descriptionElement = screen.queryByTestId("content-description");
    expect(descriptionElement).toHaveTextContent(description);
  });
});
